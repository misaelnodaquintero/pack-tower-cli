# pack-tower-cli

### Description : 

This pack  uses a python script to contact with tower-cli API and launch a template job.

the script in action/create_vm.py is a script with contact with a AWX instance and launch a job 

the yaml file in action/create_vm.yaml is a file for configuring an action with stackstorm Hubot

the yaml file in aliases/create_vm.yaml is a file for create a command available in hubot to connect slack 


### Requirements 

The file config.schema.yaml is used to store variables in the config pack ( later , you must fill these variables in the config when instanciate stackstorm)
If you want use this pack , Stackstorm will install automatically all the dependencies that found on "requirements.txt"

### How install a package via Stackstorm

in this [link](https://docs.stackstorm.com/packs.html) you will have the documentation to install a custom package in a stackstorm instance.




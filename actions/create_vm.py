#!/usr/bin/env python

import tower_cli
from tower_cli.conf import settings
from tower_cli.exceptions import Found
from st2common.runners.base_action import Action



class MyTestActionAction(Action):
    def run(self,awx_username,awx_password,awx_host,vm_name):
        variables=["resource_group_name : Roshar","location_name : northeurope","vm_name : "+ vm_name ,"admin_user : Kaladin","admin_password : Cenhelm2"]
        with settings.runtime_values(username=awx_username,password=awx_password,host=awx_host):
            try:
                tower_cli.get_resource('job').launch(job_template=12,extra_vars=variables)
                print('la orden se ha ejecutado correctamente')
            except Found:
                print('No hemos podido lanzar su trabajo')
